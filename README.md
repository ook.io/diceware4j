README.md
=====

[![Javadocs](http://www.javadoc.io/badge/io.ook/diceware4j.svg)](http://www.javadoc.io/doc/io.ook/diceware4j)

What is this all about?
-----
This project provides a simple Java wrapper for Diceware word lists, allowing developers a quick method of matching dice rolls to words without needing to manually parse the wordlist.

What is Diceware?
-----
Diceware is a technique for generating secure passphrases using dice rolls. This technique was created by Arnold G. Reinhold and documented in full at <http://world.std.com/~reinhold/dicewarefaq.html>

***WARNING***

As pointed out in the URL above, the Diceware method should really be conducting using a truly random source, such as casino-grade dice. The use of electronic random number generation ***may*** not provide truly random numbers which (if true) would compromise the security of any generated passphrases.

This project is provided without warranty of any kind. Using this library to generate passphrases is entirely at the users own risk.

How do I use it?
-----
Diceware4j is available from the Maven Central Repository. Just add the library as a Maven dependency:

~~~~~
<dependency>
	<groupId>io.ook</groupId>
	<artifactId>diceware4j</artifactId>
	<version>${diceware4j.version}</version>
</dependency>
~~~~~

Command Line Utility
-----
There is also a simple command line utility in the self-contained `-nodep.jar` that can be called directly, or used in scripts. Example:

~~~~~
java -jar -diceware4j-<version>-nodep.jar <options>
~~~~~

For example:

~~~~~
$ java -jar target/diceware4j-0.1.1-SNAPSHOT-nodep.jar
mite slug nasal knows marlin recur
~~~~~
or:

~~~~~
$ java -jar target/diceware4j-0.1.1-SNAPSHOT-nodep.jar --length 10 --verbose
Key             : 34422 34435 43244 34124 53532 44524 51514 44254 12322 32525
Passphrase      : ji jinx nausea iraq sighs pair ricky opal aorta herbs
~~~~~
Documentation
-----
Javadoc of the latest version available at: [https://javadoc.io/doc/io.ook/diceware4j/](https://javadoc.io/doc/io.ook/diceware4j/)