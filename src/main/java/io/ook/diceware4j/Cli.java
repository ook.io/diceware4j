package io.ook.diceware4j;

/**
 * The Class Cli.
 *
 * Command line utility to generate random passphrases.
 * 
 * @author mkociak
 * @version 0.1.1
 */
public final class Cli {

	/** The Constant DEFAULT_LENGTH defines how many d6 dice are required. */
	private static final int DEFAULT_LENGTH = 6;

	/** The key length. */
	private int key_length = DEFAULT_LENGTH;

	/** The verbosity flag. */
	private boolean verbose = false;

	/** The help only flag. */
	private boolean helpOnly = false;

	/**
	 * Instantiates a new Cli.
	 *
	 * @param args
	 *            the command line arguments
	 */
	public Cli(String[] args) {
		for (int i = 0; i < args.length; i++) {
			if (args[i].equalsIgnoreCase("-h") || args[i].equalsIgnoreCase("--help")
					|| args[i].equalsIgnoreCase("/?")) {
				helpOnly = true;
			} else if (args[i].equalsIgnoreCase("-v") || args[i].equalsIgnoreCase("--verbose")) {
				verbose = true;
			} else if ((args[i].equalsIgnoreCase("-l") || args[i].equalsIgnoreCase("--length")) && i < args.length) {
				try {
					key_length = Integer.parseInt(args[i + 1]);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the command line arguments
	 */
	public final static void main(final String[] args) {
		System.out.println(new Cli(args).run());
	}

	/**
	 * Run.
	 *
	 * Execute the class.
	 *
	 * @return the program output
	 */
	protected final StringBuilder run() {

		final StringBuilder sbOutput = new StringBuilder();

		if (helpOnly) {
			sbOutput.append(showUsage());
		} else {

			final Passphrase passphrase = new PassphraseGenerator(new WordlistFactory().getWordlist())
					.generatePassphrase(key_length);

			if (verbose) {
				sbOutput.append("Key\t\t: " + passphrase.getKey());
				sbOutput.append("\n");
				sbOutput.append("Passphrase\t: " + passphrase.getPassphrase());
			} else {
				sbOutput.append(passphrase.getPassphrase());
			}
		}

		return sbOutput;
	}

	/**
	 * Show usage.
	 *
	 * @return the string builder
	 */
	private final StringBuilder showUsage() {
		final StringBuilder sbUsage = new StringBuilder();

		sbUsage.append("Usage: java -jar diceware4j-<version>-nodep.jar <options>");
		sbUsage.append("\n");
		sbUsage.append("Options:");
		sbUsage.append("\n");
		sbUsage.append("\t-h, --help, /?\tShow this help");
		sbUsage.append("\n");
		sbUsage.append("\t-v, --verbose\tShow dice rolls and passphrase");
		sbUsage.append("\n");
		sbUsage.append("\t-l [LENGTH]\tSpecify passphrase word length");
		sbUsage.append("\n");

		return sbUsage;
	}

}
