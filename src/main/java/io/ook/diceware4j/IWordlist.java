/*
 * Diceware4j: Java library for management of Diceware wordlists
 * Copyright (C) 2015  Michal Kociak
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 *  PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.ook.diceware4j;

import java.util.Map;

public interface IWordlist<K, V> extends Map<Integer, String> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#containsKey(java.lang.Integer)
	 */
	boolean containsKey(Integer key);

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#containsValue(java.lang.String)
	 */
	boolean containsValue(String value);

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#get(java.lang.Integer)
	 */
	String get(Integer key);

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#put(java.lang.Integer, java.lang.String)
	 */
	String put(Integer key, String value);

	void putAll(Map<? extends Integer, ? extends String> m);
}
