/*
 * Diceware4j: Java library for management of Diceware wordlists
 * Copyright (C) 2015  Michal Kociak
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 *  PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.ook.diceware4j;

/**
 * The Class Passphrase.
 */
public final class Passphrase {

	/** The key String holds the apace delimited "dice rolls". */
	private final String key;

	/** The passphrase. */
	private final String passphrase;

	/**
	 * Instantiates a new passphrase.
	 *
	 * @param key
	 *            the key
	 * @param passphrase
	 *            the passphrase
	 */
	public Passphrase(final String key, final String passphrase) {
		this.key = key;
		this.passphrase = passphrase;
	}

	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	public final String getKey() {
		return key;
	}

	/**
	 * Gets the passphrase.
	 *
	 * @return the passphrase
	 */
	public final String getPassphrase() {
		return passphrase;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((passphrase == null) ? 0 : passphrase.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		
		if (obj == null ) {
			return false;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		Passphrase other = (Passphrase) obj;
		if (key == null) {{
			if (other.key != null) {
				return false;
			}
		}
		} else if (!key.equals(other.key)) {
			return false;
		}
		if (passphrase == null) {
			if (other.passphrase != null) {
				return false;
			}
		} else if (!passphrase.equals(other.passphrase)) {
			return false;
		}
		
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Passphrase [key=" + key + ", passphrase=" + passphrase + "]";
	}
}
