/*
 * Diceware4j: Java library for management of Diceware wordlists
 * Copyright (C) 2015  Michal Kociak
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 *  PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.ook.diceware4j;

import java.security.SecureRandom;

/**
 * The Class PassphraseGenerator.
 */
public class PassphraseGenerator implements IPassphraseGenerator {

	/** The random number source. */
	private volatile SecureRandom secureRandom = new SecureRandom();

	/** The wordlist. */
	private final IWordlist<Integer, String> wordlist;

	/**
	 * Instantiates a new passphrase generator.
	 *
	 * @param wordlist
	 *            the wordlist
	 */
	public PassphraseGenerator(final IWordlist<Integer, String> wordlist) {
		this.wordlist = wordlist;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see io.ook.diceware4j.IPassphraseGenerator#generatePassphrase(int)
	 */
	public final Passphrase generatePassphrase(final int length) {

		if (length < 1) {
			throw new IllegalArgumentException("Length of passphrase must be greater than 0");
		}

		Integer key = getRandomKey();

		final StringBuilder sbKey = new StringBuilder(key.toString());
		final StringBuilder sbpassphrase = new StringBuilder(wordlist.get(key));

		for (int i = 1; i < length; i++) {

			key = getRandomKey();

			sbKey.append(' ').append(key);
			sbpassphrase.append(' ').append(wordlist.get(key));
		}

		return new Passphrase(sbKey.toString(), sbpassphrase.toString());
	}

	/**
	 * Gets Random key for IWordlist.
	 * 
	 * @return a 5 digit integer using only the digits 1 to 6 inclusive.
	 */
	private final Integer getRandomKey() {
		final StringBuilder sbKey = new StringBuilder();

		for (int i = 1; i <= 5; i++) {
			sbKey.append(secureRandom.nextInt(5) + 1);
		}

		return new Integer(sbKey.toString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see io.ook.diceware4j.IPassphraseGenerator#getWordlist()
	 */
	public final IWordlist<Integer, String> getWordlist() {
		return this.wordlist;
	}

}
