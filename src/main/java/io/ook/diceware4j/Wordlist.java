/*
 * Diceware4j: Java library for management of Diceware wordlists
 * Copyright (C) 2015  Michal Kociak
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 *  PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.ook.diceware4j;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * The Class Wordlist.
 *
 * @param <K>
 *            the key type
 * @param <V>
 *            the value type
 */
public class Wordlist<K, V> implements IWordlist<Integer, String> {

	/** The Constant MSG_IMMUTABLE. */
	private static final String MSG_IMMUTABLE = "Wordlist is immutable";

	/** The word map. */
	private final Map<Integer, String> wordMap = new HashMap<Integer, String>();

	/**
	 * Instantiates a new wordlist.
	 *
	 * @param wordMap
	 *            the word map
	 */
	public Wordlist(final Map<Integer, String> wordMap) {
		this.wordMap.putAll(wordMap);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see io.ook.diceware4j.IWordlist#get(java.lang.Integer)
	 */
	public String get(Integer key) {
		return wordMap.get(key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see io.ook.diceware4j.IWordlist#put(java.lang.Integer, java.lang.String)
	 */
	public String put(Integer key, String value) {
		throw new UnsupportedOperationException(MSG_IMMUTABLE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see io.ook.diceware4j.IWordlist#containsKey(java.lang.Integer)
	 */
	public boolean containsKey(Integer key) {
		return wordMap.containsKey(key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see io.ook.diceware4j.IWordlist#containsValue(java.lang.String)
	 */
	public boolean containsValue(String value) {
		return wordMap.containsValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#remove(java.lang.Object)
	 */
	public String remove(Object key) {
		throw new UnsupportedOperationException(MSG_IMMUTABLE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#entrySet()
	 */
	public Set<Entry<Integer, String>> entrySet() {
		return Collections.unmodifiableSet(wordMap.entrySet());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#keySet()
	 */
	public Set<Integer> keySet() {
		return Collections.unmodifiableSet(wordMap.keySet());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see io.ook.diceware4j.IWordlist#putAll(java.util.Map)
	 */
	public void putAll(Map<? extends Integer, ? extends String> m) {
		throw new UnsupportedOperationException(MSG_IMMUTABLE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#isEmpty()
	 */
	public boolean isEmpty() {
		return wordMap.isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#size()
	 */
	public int size() {
		return wordMap.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#values()
	 */
	public Collection<String> values() {
		return Collections.unmodifiableCollection(wordMap.values());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#clear()
	 */
	public void clear() {
		throw new UnsupportedOperationException(MSG_IMMUTABLE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#get(java.lang.Object)
	 */
	public String get(Object key) {
		return wordMap.get(key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#containsKey(java.lang.Object)
	 */
	public boolean containsKey(Object key) {
		return wordMap.containsKey(key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#containsValue(java.lang.Object)
	 */
	public boolean containsValue(Object value) {
		return wordMap.containsValue(value);
	}
}
