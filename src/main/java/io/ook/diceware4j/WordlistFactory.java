/*
 * Diceware4j: Java library for management of Diceware wordlists
 * Copyright (C) 2015  Michal Kociak
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 *  PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.ook.diceware4j;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WordlistFactory {

	private static final String REGEX_GROUP_WORD = "word";

	private static final String REGEX_GROUP_DICE = "dice";

	private static final Logger LOG = LoggerFactory.getLogger(WordlistFactory.class);

	/**
	 * Wordlist key/word line pattern regular expression
	 * <code>^([1-6]{5})\s+([^\s]+)$</code>. Essentially, a 1+ digit number (using
	 * only the integers 1 to 6 inclusive), any whitespace followed by the word. The
	 * "word" is then the remainder of the line, but must not include any
	 * whitespace.
	 */
	private static final Pattern DICEWARE_PATTERN = Pattern.compile("^(?<dice>[1-6]{1,})\\s+(?<word>[^\\s]+)$");

	/**
	 * The default "Beale" Wordlist taken from this URL:
	 * <a href="http://world.std.com/~reinhold/diceware.wordlist.asc" target=
	 * "_blank">http://world.std.com/~reinhold/diceware.wordlist.asc</a>
	 */
	private static final String DEFAULT_WORDLSIT = "static/beale.wordlist.asc";

	/**
	 * Get the default built-in "Beale" Wordlist
	 * 
	 * @return the Wordlist
	 */
	public final IWordlist<Integer, String> getWordlist() {

		LOG.trace("Loading default Wordlist");

		IWordlist<Integer, String> wordlist = null;

		final InputStream wordlistStream = this.getClass().getClassLoader().getResourceAsStream(DEFAULT_WORDLSIT);

		try {
			wordlist = new Wordlist<Integer, String>(loadWords(wordlistStream));
		} finally {
			try {
				wordlistStream.close();
			} catch (IOException e) {
				LOG.error("Problem closing wordlist stream", e);
			}
		}
		return wordlist;
	}

	/**
	 * Get the Wordlist from the specified File. Extracted lines must match the
	 * DICEWARE_PATTERN
	 * 
	 * @param customWordlist
	 *            the File to parse
	 * @return the Wordlist
	 * @throws FileNotFoundException
	 *             if the supplied file does not exist or cannot be accessed
	 */
	public final IWordlist<Integer, String> getWordlist(final File customWordlist) throws FileNotFoundException {

		LOG.trace("Loading Wordlist from File");

		if (customWordlist == null || customWordlist.exists() == false) {
			throw new FileNotFoundException();
		}

		IWordlist<Integer, String> wordlist = null;

		final FileInputStream wordlistStream = new FileInputStream(customWordlist);

		try {
			wordlist = new Wordlist<Integer, String>(loadWords(wordlistStream));
		} finally {
			try {
				wordlistStream.close();
			} catch (IOException e) {
				LOG.error("Problem closing wordlist stream", e);
			}
		}
		return wordlist;
	}

	/**
	 * Get the Wordlist from the specified URL. Extracted lines must match the
	 * DICEWARE_PATTERN
	 *
	 * @param customWordlist
	 *            the URL of the Wordlist to parse
	 * @return the Wordlist
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public final IWordlist<Integer, String> getWordlist(final URL customWordlist) throws IOException {

		LOG.trace("Loading Wordlist from URL");

		IWordlist<Integer, String> wordlist = null;

		final InputStream wordlistStream = customWordlist.openStream();

		try {
			wordlist = new Wordlist<Integer, String>(loadWords(wordlistStream));
		} finally {
			try {
				wordlistStream.close();
			} catch (IOException e) {
				LOG.error("Problem closing wordlist stream", e);
			}
		}
		return wordlist;
	}

	/*
	 * Returns a Wordlist from the classpath using the specified package/file
	 * location. Extracted lines must match the DICEWARE_PATTERN
	 * 
	 * @param customWordlist The classpath package/location
	 * 
	 * @return the Wordlist
	 */
	public final IWordlist<Integer, String> getWordlist(final String customWordlist) {

		LOG.trace("Loading Wordlist from classpath");

		IWordlist<Integer, String> wordlist = null;

		final InputStream wordlistStream = this.getClass().getClassLoader().getResourceAsStream(customWordlist);

		if (wordlistStream == null) {
			throw new IllegalArgumentException();
		}

		try {
			wordlist = new Wordlist<Integer, String>(loadWords(wordlistStream));
		} finally {
			try {
				wordlistStream.close();
			} catch (IOException e) {
				LOG.error("Problem closing wordlist stream", e);
			}
		}
		return wordlist;
	}

	/**
	 * Returns a Wordlist based on the supplied InputStream.
	 * 
	 * Note that unlike the other factory methods that return Wordlist, this one
	 * will not close the supplied java.io.InputStream
	 * 
	 * @param customWordlistStream
	 *            the InputStream to parse
	 * @return the Wordlist instance
	 */
	public final IWordlist<Integer, String> getWordlist(final InputStream customWordlistStream) {
		LOG.trace("Loading Wordlist from InputStream");
		return new Wordlist<Integer, String>(loadWords(customWordlistStream));
	}

	/**
	 * Loads words from InputStream and returns the Map.
	 * 
	 * This is a convenience method that defaults to UTF-8 character set.
	 * 
	 * @param is
	 *            the InputStream to parse
	 * @return The word Map
	 */
	protected final Map<Integer, String> loadWords(final InputStream is) {
		return loadWords(is, StandardCharsets.UTF_8);
	}

	/**
	 * Loads words from InputStream and returns the Map.
	 * 
	 * This is a convenience method that defaults to UTF-8 character set.
	 * 
	 * @param is
	 *            the InputStream to parse
	 * @param charset
	 *            the character set encoding used by the supplied InputStream
	 * @return the word Map
	 */
	protected final Map<Integer, String> loadWords(InputStream is, Charset charset) {

		final Map<Integer, String> wordMap = new HashMap<Integer, String>();

		final BufferedReader br = new BufferedReader(new InputStreamReader(is, charset));

		try {
			String line = br.readLine();

			while (line != null) {

				Matcher matcher = DICEWARE_PATTERN.matcher(line);

				if (matcher.matches()) {
					wordMap.put(Integer.parseInt(matcher.group(REGEX_GROUP_DICE)), matcher.group(REGEX_GROUP_WORD));
				}

				line = br.readLine();
			}
		} catch (IOException e) {
			LOG.error("IOException with diceware wordlist", e);
		} finally {
			if (br != null) {
				 try {
				 br.close();
				 } catch (IOException e) {
				 LOG.error("Problem closing wordlist stream", e);
				 }
			}
		}

		LOG.debug("Loaded {0} diceware words.", new Object[] { wordMap.size() });

		return wordMap;
	}

}
