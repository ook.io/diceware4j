package io.ook.diceware4j;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

public class TestCli {

	@Test
	public final void testUsageShort() {
		String[] args = new String[] { "-h" };

		StringBuilder sbOutput = new Cli(args).run();
		assertThat(sbOutput.toString().startsWith("Usage:"), equalTo(true));
	}

	@Test
	public final void testUsageLong() {
		String[] args = new String[] { "--help" };

		StringBuilder sbOutput = new Cli(args).run();
		assertThat(sbOutput.toString().startsWith("Usage:"), equalTo(true));
	}

	@Test
	public final void testUsageDOSStyle() {
		String[] args = new String[] { "/?" };

		StringBuilder sbOutput = new Cli(args).run();
		assertThat(sbOutput.toString().startsWith("Usage:"), equalTo(true));
	}

	@Test
	public final void testNoArgs() {
		String[] args = new String[] {};

		StringBuilder sbOutput = new Cli(args).run();

		Matcher matcher = Pattern.compile("^([^\\s]+ ){5}[^\\s]+").matcher(sbOutput.toString());

		assertThat(matcher.matches(), equalTo(true));
	}

	@Test(expected = IllegalArgumentException.class)
	public final void testLengthZero() {
		String[] args = new String[] { "--length", "0" };

		@SuppressWarnings("unused")
		StringBuilder sbOutput = new Cli(args).run();
	}

	@Test
	public final void testLengthShortTen() {
		String[] args = new String[] { "-l", "10" };

		StringBuilder sbOutput = new Cli(args).run();

		Matcher matcher = Pattern.compile("^([^\\s]+ ){9}[^\\s]+").matcher(sbOutput.toString());

		assertThat(matcher.matches(), equalTo(true));
	}

	@Test
	public final void testLengthLongTen() {
		String[] args = new String[] { "--length", "10" };

		StringBuilder sbOutput = new Cli(args).run();

		Matcher matcher = Pattern.compile("^([^\\s]+ ){9}[^\\s]+").matcher(sbOutput.toString());

		assertThat(matcher.matches(), equalTo(true));
	}

	@Test
	public final void testVerboseShort() {
		String[] args = new String[] { "-v" };

		String output = new Cli(args).run().toString();

		StringBuilder sbVerboseRegex = new StringBuilder();
		sbVerboseRegex.append("^Key[\\s]+:\\s").append("(\\d{5} ){5}\\d{5}");
		sbVerboseRegex.append("\\n");
		sbVerboseRegex.append("Passphrase[\\s]+:\\s").append("([^\\s]+ ){5}[^\\s]+$");

		Pattern p = Pattern.compile(sbVerboseRegex.toString(), Pattern.DOTALL);

		Matcher matcher = p.matcher(output);

		assertThat(matcher.matches(), equalTo(true));
	}

	@Test
	public final void testVerboseLong() {
		String[] args = new String[] { "--verbose" };

		String output = new Cli(args).run().toString();

		StringBuilder sbVerboseRegex = new StringBuilder();
		sbVerboseRegex.append("^Key[\\s]+:\\s").append("(\\d{5} ){5}\\d{5}");
		sbVerboseRegex.append("\\n");
		sbVerboseRegex.append("Passphrase[\\s]+:\\s").append("([^\\s]+ ){5}[^\\s]+$");

		Pattern p = Pattern.compile(sbVerboseRegex.toString(), Pattern.DOTALL);

		Matcher matcher = p.matcher(output);

		assertThat(matcher.matches(), equalTo(true));
	}
}
