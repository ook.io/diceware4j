/*
 * Diceware4j: Java library for management of Diceware wordlists
 * Copyright (C) 2015  Michal Kociak
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 *  PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.ook.diceware4j;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.io.FileNotFoundException;

import org.junit.Test;

/**
 * The Class TestPassphraseGenerator.
 *
 * @author mkociak
 */
public class TestPassphraseGenerator {

	/**
	 * Test method for
	 * {@link io.ook.diceware4j.PassphraseGenerator#PassphraseGenerator(io.ook.diceware4j.IWordlist)}.
	 *
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	@Test
	public void testPassphraseGenerator() throws FileNotFoundException {
		final File testFile = new File("src/main/resources/static/beale.wordlist.asc");
		final IWordlist<Integer, String> wordlist = new WordlistFactory().getWordlist(testFile);
		final PassphraseGenerator ppg = new PassphraseGenerator(wordlist);
		assertThat(ppg.getWordlist(), equalTo(wordlist));
	}

	/**
	 * Test method for
	 * {@link io.ook.diceware4j.PassphraseGenerator#generatePassphrase(int)}.
	 *
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	@Test
	public void testGeneratePassphrase() throws FileNotFoundException {
		final File testFile = new File("src/main/resources/static/beale.wordlist.asc");
		final IWordlist<Integer, String> wordlist = new WordlistFactory().getWordlist(testFile);
		final PassphraseGenerator ppg = new PassphraseGenerator(wordlist);
		final Passphrase pp = ppg.generatePassphrase(6);
		assertThat(pp, instanceOf(Passphrase.class));
		assertThat(pp.getKey().split(" ").length, equalTo(6));
		assertThat(pp.getPassphrase().split(" ").length, equalTo(6));
	}

	/**
	 * Test method for
	 * {@link io.ook.diceware4j.PassphraseGenerator#generatePassphrase(int)}.
	 *
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGeneratePassphraseTooShort() throws FileNotFoundException {
		final File testFile = new File("src/main/resources/static/beale.wordlist.asc");
		final IWordlist<Integer, String> wordlist = new WordlistFactory().getWordlist(testFile);
		final PassphraseGenerator ppg = new PassphraseGenerator(wordlist);
		@SuppressWarnings("unused")
		Passphrase pp = ppg.generatePassphrase(0);
	}

	/**
	 * Test method for {@link io.ook.diceware4j.PassphraseGenerator#getWordlist()}.
	 *
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	@Test
	public void testGetWordlist() throws FileNotFoundException {
		final File testFile = new File("src/main/resources/static/beale.wordlist.asc");
		final IWordlist<Integer, String> wordlist = new WordlistFactory().getWordlist(testFile);
		final PassphraseGenerator ppg = new PassphraseGenerator(wordlist);
		assertThat(ppg.getWordlist(), equalTo(wordlist));
	}
}
