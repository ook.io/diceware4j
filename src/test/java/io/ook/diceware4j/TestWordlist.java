/*
 * Diceware4j: Java library for management of Diceware wordlists
 * Copyright (C) 2015  Michal Kociak
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 *  PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.ook.diceware4j;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

/**
 * The Class TestWordlist.
 *
 * @author mkociak
 */
public final class TestWordlist {

	/** The Constant wordMap. */
	private static final Map<Integer, String> wordMap = new HashMap<Integer, String>();

	/**
	 * Before test.
	 */
	@Before
	public final void beforeTest() {
		wordMap.put(new Integer(1), "test1");
		wordMap.put(new Integer(2), "test2");
		wordMap.put(new Integer(3), "test3");
		wordMap.put(new Integer(4), "test4");
		wordMap.put(new Integer(5), "test5");
		wordMap.put(new Integer(6), "test6");
	}

	/**
	 * Test method for {@link io.ook.diceware4j.Wordlist#size()}.
	 */
	@Test
	public final void testSize() {
		final IWordlist<Integer, String> wordlist = new Wordlist<Integer, String>(wordMap);
		assertThat(wordlist.size(), equalTo(6));
	}

	/**
	 * Test method for {@link io.ook.diceware4j.Wordlist#isEmpty()}.
	 */
	@Test
	public final void testIsEmpty() {
		final IWordlist<Integer, String> wordlist = new Wordlist<Integer, String>(wordMap);
		assertThat(wordlist.isEmpty(), equalTo(false));
	}

	/**
	 * Test method for
	 * {@link io.ook.diceware4j.Wordlist#containsKey(java.lang.Integer)}.
	 */
	@Test
	public final void testContainsKeyInteger() {
		final IWordlist<Integer, String> wordlist = new Wordlist<Integer, String>(wordMap);
		assertThat(wordlist.containsKey(new Integer(1)), equalTo(true));
	}

	/**
	 * Test method for
	 * {@link io.ook.diceware4j.Wordlist#containsKey(java.lang.Integer)}.
	 */
	@Test
	public final void testContainsKeyIntegerInvalid() {
		final IWordlist<Integer, String> wordlist = new Wordlist<Integer, String>(wordMap);
		assertThat(wordlist.containsKey(new Integer(7)), equalTo(false));
	}

	/**
	 * Test method for
	 * {@link io.ook.diceware4j.Wordlist#containsValue(java.lang.String)}.
	 */
	@Test
	public final void testContainsValueString() {
		final IWordlist<Integer, String> wordlist = new Wordlist<Integer, String>(wordMap);
		assertThat(wordlist.containsValue("test1"), equalTo(true));
	}

	/**
	 * Test method for
	 * {@link io.ook.diceware4j.Wordlist#containsValue(java.lang.String)}.
	 */
	@Test
	public final void testContainsValueStringInvalid() {
		final IWordlist<Integer, String> wordlist = new Wordlist<Integer, String>(wordMap);
		assertThat(wordlist.containsValue("test7"), equalTo(false));
	}

	/**
	 * Test method for {@link io.ook.diceware4j.Wordlist#get(java.lang.Integer)}.
	 */
	@Test
	public final void testGetInteger() {
		final IWordlist<Integer, String> wordlist = new Wordlist<Integer, String>(wordMap);
		assertThat(wordlist.get(new Integer(1)), equalTo("test1"));
	}

	/**
	 * Test method for {@link io.ook.diceware4j.Wordlist#keySet()}.
	 */
	@Test
	public final void testKeySet() {
		final IWordlist<Integer, String> wordlist = new Wordlist<Integer, String>(wordMap);
		assertThat(wordlist.keySet(), hasSize(6));
	}

	/**
	 * Test method for {@link io.ook.diceware4j.Wordlist#values()}.
	 */
	@Test
	public final void testValues() {
		final IWordlist<Integer, String> wordlist = new Wordlist<Integer, String>(wordMap);
		assertThat(wordlist.values(), hasSize(6));
	}

	/**
	 * Test method for {@link io.ook.diceware4j.Wordlist#entrySet()}.
	 */
	@Test
	public final void testEntrySet() {
		final IWordlist<Integer, String> wordlist = new Wordlist<Integer, String>(wordMap);
		assertThat(wordlist.entrySet(), hasSize(6));
	}

	/**
	 * Test method for
	 * {@link io.ook.diceware4j.Wordlist#put(java.lang.Integer, java.lang.String)}.
	 */
	@Test(expected = UnsupportedOperationException.class)
	public final void testPut() {
		final IWordlist<Integer, String> wordlist = new Wordlist<Integer, String>(wordMap);
		wordlist.put(new Integer(7), "test7");
	}

	/**
	 * Test method for {@link io.ook.diceware4j.Wordlist#remove(java.lang.Object)}.
	 */
	@Test(expected = UnsupportedOperationException.class)
	public final void testRemove() {
		final IWordlist<Integer, String> wordlist = new Wordlist<Integer, String>(wordMap);
		wordlist.remove(new Integer(1));
	}

	/**
	 * Test method for {@link io.ook.diceware4j.Wordlist#putAll(java.util.Map)}.
	 */
	@Test(expected = UnsupportedOperationException.class)
	public final void testPutAll() {
		final IWordlist<Integer, String> wordlist = new Wordlist<Integer, String>(wordMap);
		wordlist.putAll(wordMap);
	}

	/**
	 * Test method for {@link io.ook.diceware4j.Wordlist#clear()}.
	 */
	@Test(expected = UnsupportedOperationException.class)
	public final void testClear() {
		final IWordlist<Integer, String> wordlist = new Wordlist<Integer, String>(wordMap);
		wordlist.clear();
	}

	/**
	 * Test method for {@link io.ook.diceware4j.Wordlist#get(java.lang.Object)}.
	 */
	@Test
	public final void testGetObject() {
		final IWordlist<Integer, String> wordlist = new Wordlist<Integer, String>(wordMap);
		assertThat(wordlist.get((Object) new Integer(1)), equalTo("test1"));
	}

	/**
	 * Test method for {@link io.ook.diceware4j.Wordlist#get(java.lang.Object)}.
	 */
	@SuppressWarnings("unlikely-arg-type")
	@Test
	public final void testGetObjectInvalid() {
		final IWordlist<Integer, String> wordlist = new Wordlist<Integer, String>(wordMap);
		assertThat(wordlist.get("invalid key"), nullValue());
	}

	/**
	 * Test method for
	 * {@link io.ook.diceware4j.Wordlist#containsKey(java.lang.Object)}.
	 */
	@Test
	public final void testContainsKeyObject() {
		final IWordlist<Integer, String> wordlist = new Wordlist<Integer, String>(wordMap);
		assertThat(wordlist.containsKey(new Integer(1)), equalTo(true));
	}

	/**
	 * Test method for
	 * {@link io.ook.diceware4j.Wordlist#containsKey(java.lang.Object)}.
	 */
	@SuppressWarnings("unlikely-arg-type")
	@Test
	public final void testContainsKeyObjectInvalid() {
		final IWordlist<Integer, String> wordlist = new Wordlist<Integer, String>(wordMap);
		assertThat(wordlist.containsKey("Invalid key"), equalTo(false));
	}

	/**
	 * Test method for
	 * {@link io.ook.diceware4j.Wordlist#containsValue(java.lang.Object)}.
	 */
	@Test
	public final void testContainsValueObject() {
		final IWordlist<Integer, String> wordlist = new Wordlist<Integer, String>(wordMap);
		assertThat(wordlist.containsValue("test1"), equalTo(true));
	}

	/**
	 * Test method for
	 * {@link io.ook.diceware4j.Wordlist#containsValue(java.lang.Object)}.
	 */
	@SuppressWarnings("unlikely-arg-type")
	@Test
	public final void testContainsValueObjectInvalid() {
		final IWordlist<Integer, String> wordlist = new Wordlist<Integer, String>(wordMap);
		assertThat(wordlist.containsValue(new Integer(1)), equalTo(false));
	}
}
