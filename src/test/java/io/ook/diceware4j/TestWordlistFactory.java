/*
 * Diceware4j: Java library for management of Diceware wordlists
 * Copyright (C) 2015  Michal Kociak
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 *  PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.ook.diceware4j;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.junit.Test;

/**
 * A factory for creating TestWordlist objects.
 *
 * @author mkociak
 */
public final class TestWordlistFactory {

	/** The Constant DEFAULT_WORDLIST_SIZE. */
	private static final Integer DEFAULT_WORDLIST_SIZE = new Integer(7776);

	/**
	 * Test method for {@link io.ook.diceware4j.WordlistFactory#getWordlist()}.
	 */
	@Test
	public final void testGetWordlistDefault() {
		final IWordlist<Integer, String> wordlist = new WordlistFactory().getWordlist();
		assertThat(wordlist.keySet(), hasSize(new Integer(DEFAULT_WORDLIST_SIZE)));
	}

	/**
	 * Test method for
	 * {@link io.ook.diceware4j.WordlistFactory#getWordlist(java.io.File)}.
	 *
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	@Test
	public final void testGetWordlistFile() throws FileNotFoundException {
		final File testFile = new File("src/main/resources/static/beale.wordlist.asc");
		IWordlist<Integer, String> wordlist = new WordlistFactory().getWordlist(testFile);
		assertThat(wordlist.keySet(), hasSize(new Integer(DEFAULT_WORDLIST_SIZE)));
	}

	/**
	 * Test method for
	 * {@link io.ook.diceware4j.WordlistFactory#getWordlist(java.io.File)}.
	 *
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	@Test(expected = FileNotFoundException.class)
	public final void testGetWordlistFileInvalidFile() throws FileNotFoundException {
		final File testFile = new File("src/main/resources/static/beale.wordlist.asc_NO_SUCH_FILE");
		@SuppressWarnings("unused")
		IWordlist<Integer, String> wordlist = new WordlistFactory().getWordlist(testFile);
	}

	/**
	 * Test method for
	 * {@link io.ook.diceware4j.WordlistFactory#getWordlist(java.io.File)}.
	 *
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	@Test(expected = FileNotFoundException.class)
	public final void testGetWordlistFileNullFile() throws FileNotFoundException {
		final File testFile = null;
		@SuppressWarnings("unused")
		IWordlist<Integer, String> wordlist = new WordlistFactory().getWordlist(testFile);
	}

	/**
	 * Test method for
	 * {@link io.ook.diceware4j.WordlistFactory#getWordlist(java.net.URL)}.
	 *
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public final void testGetWordlistURL() throws IOException {
		final URL testURL = new File("src/main/resources/static/beale.wordlist.asc").toURI().toURL();
		IWordlist<Integer, String> wordlist = new WordlistFactory().getWordlist(testURL);
		assertThat(wordlist.keySet(), hasSize(new Integer(DEFAULT_WORDLIST_SIZE)));
	}

	/**
	 * Test method for
	 * {@link io.ook.diceware4j.WordlistFactory#getWordlist(java.net.URL)}.
	 *
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test(expected = IOException.class)
	public final void testGetWordlistURLInvalidURL() throws IOException {
		final URL testURL = new File("src/main/resources/static/beale.wordlist.asc_NO_SUCH_FILE").toURI().toURL();
		@SuppressWarnings("unused")
		IWordlist<Integer, String> wordlist = new WordlistFactory().getWordlist(testURL);
	}

	/**
	 * Test method for
	 * {@link io.ook.diceware4j.WordlistFactory#getWordlist(java.lang.String)}.
	 */
	@Test
	public final void testGetWordlistString() {
		final IWordlist<Integer, String> wordlist = new WordlistFactory().getWordlist("static/beale.wordlist.asc");
		assertThat(wordlist.keySet(), hasSize(new Integer(DEFAULT_WORDLIST_SIZE)));
	}

	/**
	 * Test method for
	 * {@link io.ook.diceware4j.WordlistFactory#getWordlist(java.lang.String)}.
	 */
	@Test(expected = IllegalArgumentException.class)
	public final void testGetWordlistStringInvalidClasspath() {
		final IWordlist<Integer, String> wordlist = new WordlistFactory()
				.getWordlist("static/beale.wordlist.asc_NO_SUCH_FILE");
		assertThat(wordlist.keySet(), hasSize(new Integer(DEFAULT_WORDLIST_SIZE)));
	}

	/**
	 * Test method for
	 * {@link io.ook.diceware4j.WordlistFactory#getWordlist(java.io.InputStream)}.
	 * 
	 * @throws IOException
	 */
	@Test
	public final void testGetWordlistInputStream() throws IOException {
		final FileInputStream fis = new FileInputStream("src/main/resources/static/beale.wordlist.asc");
		final IWordlist<Integer, String> wordlist = new WordlistFactory().getWordlist(fis);
		assertThat(wordlist.keySet(), hasSize(new Integer(DEFAULT_WORDLIST_SIZE)));
		fis.close();
	}

	/**
	 * Test method for
	 * {@link io.ook.diceware4j.WordlistFactory#loadDicewareWords(java.io.InputStream, java.nio.charset.Charset)}.
	 * 
	 * @throws IOException
	 */
	@Test
	public final void testLoadDicewareWordsInputStreamCharset() throws IOException {
		final FileInputStream fis = new FileInputStream("src/main/resources/static/beale.wordlist.asc");
		final Map<Integer, String> wordlistMap = new WordlistFactory().loadWords(fis, StandardCharsets.UTF_8);
		assertThat(wordlistMap.keySet(), hasSize(new Integer(DEFAULT_WORDLIST_SIZE)));
		fis.close();
	}

	@Test
	public final void testGetWordlistOneD6() throws IOException {
		final File file = new File("src/test/resources/static/test_wordlist_one_d6.txt");
		final IWordlist<Integer, String> wordlist = new WordlistFactory().getWordlist(file);
		assertThat(wordlist.keySet(), hasSize(6));
		assertThat(wordlist.get(new Integer(1)), equalTo("This"));
		assertThat(wordlist.get(new Integer(2)), equalTo("is"));
		assertThat(wordlist.get(new Integer(3)), equalTo("a"));
		assertThat(wordlist.get(new Integer(4)), equalTo("test"));
		assertThat(wordlist.get(new Integer(5)), equalTo("Diceware"));
		assertThat(wordlist.get(new Integer(6)), equalTo("wordlist"));
	}

}
